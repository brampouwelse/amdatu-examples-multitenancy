package org.amdatu.example.multitenancy.user;

import java.util.List;

public interface UserService {

	User get(String username); 
	
	List<User> list();
	
	void add(User user);
	
}
