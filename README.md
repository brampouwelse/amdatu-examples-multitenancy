# Amdatu Multitenancy Example

This is an Amdatu example project. This project represents an issue tracking system with an optional worklog plugin that can be used by multiple tenants.

## Technology included

   - Amdatu Multitenancy
   - Amdatu Web  
   - Amdatu JPA
   - Amdatu Configurator


## The application

The example application represents an issue tracking system with three modules (users, issues and worklogs). The worklog module is optional for tenants.

The workspace has 3 tenants configured

  - A Inc. (users, isses)
  - B Corp. (users, issues, worklog)
  - C & Sons.** (users , issues)

** C & Sons is the customer we all love. They don't use the default user management but have a custom solution (in memory for this example). And they require that all issue summary's are UPPERCASE.

To run the example just hit run (run/run.bndrun) and there is a basic web interface available at http://localhost:8080/user/
