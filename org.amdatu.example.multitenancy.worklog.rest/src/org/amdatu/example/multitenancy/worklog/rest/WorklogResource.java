package org.amdatu.example.multitenancy.worklog.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.example.multitenancy.worklog.Worklog;
import org.amdatu.example.multitenancy.worklog.WorklogService;

@Path("worklog")
@Produces(MediaType.APPLICATION_JSON)
public class WorklogResource {

	private volatile WorklogService worklogService;
	
	@GET
	@Path("list/{userName}")
	public List<Worklog> list(@PathParam("userName") String userName){
		return worklogService.list(userName);
	}
	
	@GET
	@Path("list")
	public List<Worklog> list(){
		return worklogService.list();
	}
	
	@POST
	public Worklog add(NewWorklog newWorklog){
		return worklogService.add(newWorklog.getUser().getUserName(), newWorklog.getIssue().getId(), newWorklog.getDate(), newWorklog.getMinutesSpent());
	}
	
	
}
