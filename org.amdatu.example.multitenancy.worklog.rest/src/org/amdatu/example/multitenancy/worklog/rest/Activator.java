package org.amdatu.example.multitenancy.worklog.rest;

import org.amdatu.example.multitenancy.worklog.WorklogService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		dm.add(createComponent().setInterface(Object.class.getName(), null).setImplementation(WorklogResource.class)
				.add(createServiceDependency().setService(WorklogService.class).setRequired(true)));

	}

}
