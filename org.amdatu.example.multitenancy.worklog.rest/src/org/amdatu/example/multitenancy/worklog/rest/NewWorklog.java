package org.amdatu.example.multitenancy.worklog.rest;

import java.util.Date;

import org.amdatu.example.multitenancy.issue.Issue;
import org.amdatu.example.multitenancy.user.User;

public class NewWorklog {
	
	private User user; 
	
	private Issue issue;
	
	private Date date; 
	
	private Integer minutesSpent;

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Issue getIssue() {
		return issue;
	}

	public void setIssue(Issue issue) {
		this.issue = issue;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public Integer getMinutesSpent() {
		return minutesSpent;
	}

	public void setMinutesSpent(Integer minutesSpent) {
		this.minutesSpent = minutesSpent;
	} 
	
	
	

}
