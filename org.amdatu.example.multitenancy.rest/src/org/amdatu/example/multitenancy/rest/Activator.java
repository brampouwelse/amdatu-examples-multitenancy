package org.amdatu.example.multitenancy.rest;

import java.util.Properties;

import org.amdatu.multitenant.Tenant;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		Properties props = new Properties();
		props.put("org.amdatu.tenant.pid", "org.amdatu.tenant.PLATFORM");
		dm.add(createComponent().setInterface(Object.class.getName(), null).setImplementation(TenantResource.class)
				.add(createServiceDependency().setService(Tenant.class).setRequired(false).setCallbacks("addTenant", "removeTenant")));
	}

}
