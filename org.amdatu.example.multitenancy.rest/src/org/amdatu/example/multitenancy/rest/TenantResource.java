package org.amdatu.example.multitenancy.rest;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.multitenant.Tenant;
import org.osgi.framework.ServiceReference;

@Path("tenant")
@Produces(MediaType.APPLICATION_JSON)
public class TenantResource {

	private final Set<Tenant> m_tenants = new HashSet<>();
	
	@GET
	@Path("list")
	public List<Tenant> list(){
		return new ArrayList<Tenant>(m_tenants);
	}
	
	@GET
	@Path("list/{module}")
	public List<Tenant> list(@PathParam("module") String module){
		List<Tenant> tenants = new ArrayList<Tenant>();
		
		for (Tenant e : m_tenants){
			if (e.getProperties().containsKey(module) && e.getProperties().get(module).equals("true")){
				tenants.add(e);
			}
		}
		
		return tenants;
	}
	
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void addTenant(ServiceReference<Tenant> ref, Tenant tenant){
		if (!tenant.getPID().equals("org.amdatu.tenant.PLATFORM"))
		synchronized (m_tenants) {
			m_tenants.add(tenant);
		}
	}
	
	@SuppressWarnings("unused" /* dependency manager callback */)
	private void removeTenant(Tenant tenant){
		synchronized (m_tenants) {
			m_tenants.remove(tenant);
		}
	}
	
}
