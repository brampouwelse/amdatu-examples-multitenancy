package org.amdatu.example.multitenancy.tenant.cas;

import org.amdatu.example.multitenancy.issue.IssueService;
import org.amdatu.example.multitenancy.user.UserService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {
		
	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		dm.add(createComponent().setInterface(UserService.class.getName(), null).setImplementation(CasUserService.class));
		
		dm.add(createAspectService(IssueService.class, null, 100).setImplementation(CasIssueSummaryUppercaseAspect.class));
		
	}

}
