package org.amdatu.example.multitenancy.tenant.cas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.amdatu.example.multitenancy.user.User;
import org.amdatu.example.multitenancy.user.UserService;

public class CasUserService implements UserService{

	private final Map<String, User> users = new HashMap<>();
	
	public CasUserService() {
		users.put("admin", new User("admin", "Admin user"));
		users.put("user", new User("user", "User"));
	}
	
	@Override
	public User get(String username) {
		return users.get(username);
	}

	@Override
	public List<User> list() {
		return new ArrayList<User>(users.values());
	}

	@Override
	public void add(User user) {
		users.put(user.getUserName(), user);
	}

}
