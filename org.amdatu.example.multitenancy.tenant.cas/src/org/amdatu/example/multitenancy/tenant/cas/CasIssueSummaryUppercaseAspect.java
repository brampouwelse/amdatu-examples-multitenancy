package org.amdatu.example.multitenancy.tenant.cas;

import java.util.List;

import org.amdatu.example.multitenancy.issue.Issue;
import org.amdatu.example.multitenancy.issue.IssueService;
import org.amdatu.example.multitenancy.user.User;

public class CasIssueSummaryUppercaseAspect implements IssueService {

	private volatile IssueService issueService;

	public Issue get(Integer id) {
		return issueService.get(id);
	}

	public List<Issue> list() {
		return issueService.list();
	}

	public List<Issue> list(User createdBy) {
		return issueService.list(createdBy);
	}

	public Issue create(User createdBy, String summary, String description) {
		return issueService.create(createdBy, summary.toUpperCase(), description);
	}
	
}
