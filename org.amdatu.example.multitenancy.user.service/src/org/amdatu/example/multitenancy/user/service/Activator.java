package org.amdatu.example.multitenancy.user.service;

import static org.osgi.service.jpa.EntityManagerFactoryBuilder.JPA_UNIT_NAME;

import java.util.Properties;

import javax.persistence.EntityManager;

import org.amdatu.example.multitenancy.user.UserService;
import org.amdatu.jta.ManagedTransactional;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		String entityManagerFilter = String.format("(%s=%s)", JPA_UNIT_NAME, "UserPu");

		Properties props = new Properties();
		props.put(ManagedTransactional.SERVICE_PROPERTY, UserService.class.getName());
		dm.add(createComponent().setInterface(Object.class.getName(), props).setImplementation(JpaUserService.class)
				.add(createServiceDependency().setService(EntityManager.class, entityManagerFilter).setRequired(true)));
	}

}
