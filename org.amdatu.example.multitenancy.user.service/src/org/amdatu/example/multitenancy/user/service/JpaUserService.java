package org.amdatu.example.multitenancy.user.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.amdatu.example.multitenancy.user.User;
import org.amdatu.example.multitenancy.user.UserService;
import org.amdatu.jta.Transactional;

@Transactional
public class JpaUserService implements UserService {

	private volatile EntityManager entityManager;
	
	@Override
	public User get(String username) {
		JpaUser jpaUser = entityManager.find(JpaUser.class, username);
		if (jpaUser != null){
			return new User(jpaUser.getUserName(), jpaUser.getDisplayName());
		}
		return null;
	}

	@Override
	public List<User> list() {
		TypedQuery<JpaUser> query = entityManager.createQuery("SELECT u FROM JpaUser u", JpaUser.class);
		List<User> users = new ArrayList<User>();
		for (JpaUser jpaUser : query.getResultList()) {
			users.add(new User(jpaUser.getUserName(), jpaUser.getDisplayName()));
		}
		return users;
	}

	@Override
	public void add(User user) {
		JpaUser jpaUser = new JpaUser(user.getUserName(), user.getDisplayName());
		entityManager.persist(jpaUser);
	}

}
