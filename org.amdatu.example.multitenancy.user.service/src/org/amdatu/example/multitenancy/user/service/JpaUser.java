package org.amdatu.example.multitenancy.user.service;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class JpaUser {

	@Id
	private String userName; 
	
	private String displayName;
	
	public JpaUser() {
		
	}

	public JpaUser(String userName, String displayName) {
		this.userName = userName;
		this.displayName = displayName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	
}
