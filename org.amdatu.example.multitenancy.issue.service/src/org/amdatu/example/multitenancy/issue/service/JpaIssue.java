package org.amdatu.example.multitenancy.issue.service;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class JpaIssue {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String createdByUserName;

	private String summary;
	
	private String description;

	public JpaIssue() {
	}
	
	public JpaIssue(String createdByUserName, String summary, String description) {
		this.createdByUserName = createdByUserName;
		this.summary = summary;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getCreatedByUserName() {
		return createdByUserName;
	}
	
	public void setCreatedByUserName(String createdByUserName) {
		this.createdByUserName = createdByUserName;
	}

	public String getSummary() {
		return summary;
	}
	
	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}

	
	
}
