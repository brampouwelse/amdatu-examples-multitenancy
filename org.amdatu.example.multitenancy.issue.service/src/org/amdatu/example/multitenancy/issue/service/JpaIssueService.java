package org.amdatu.example.multitenancy.issue.service;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.amdatu.example.multitenancy.issue.Issue;
import org.amdatu.example.multitenancy.issue.IssueService;
import org.amdatu.example.multitenancy.user.User;
import org.amdatu.example.multitenancy.user.UserService;
import org.amdatu.jta.Transactional;

@Transactional
public class JpaIssueService implements IssueService {

	private volatile EntityManager entityManager;

	private volatile UserService userService;

	@Override
	public Issue get(Integer id) {
		JpaIssue jpaIssue = entityManager.find(JpaIssue.class, id);

		if (jpaIssue != null) {
			return toIssue(jpaIssue);
		}

		return null;
	}

	@Override
	public List<Issue> list() {
		TypedQuery<JpaIssue> query = entityManager.createQuery("SELECT i FROM JpaIssue i", JpaIssue.class);
		return toIssueList(query);
	}

	@Override
	public List<Issue> list(User createdBy) {
		TypedQuery<JpaIssue> query = entityManager.createQuery(
				"SELECT i FROM JpaIssue i WHERE i.createdByUserName = :userName", JpaIssue.class);
		query.setParameter("userName", createdBy.getUserName());
		return toIssueList(query);
	}

	@Override
	public Issue create(User createdBy, String summary, String description) {
		JpaIssue jpaIssue = new JpaIssue(createdBy.getUserName(), summary, description);
		entityManager.persist(jpaIssue);
		return toIssue(jpaIssue);
	}

	private Issue toIssue(JpaIssue jpaIssue) {
		User user = userService.get(jpaIssue.getCreatedByUserName());
		return new Issue(jpaIssue.getId(), user, jpaIssue.getSummary(), jpaIssue.getDescription());
	}

	private List<Issue> toIssueList(TypedQuery<JpaIssue> query) {
		List<JpaIssue> jpaIssues = query.getResultList();

		List<Issue> issues = new ArrayList<Issue>();
		for (JpaIssue i : jpaIssues) {
			issues.add(toIssue(i));
		}

		return issues;
	}

}
