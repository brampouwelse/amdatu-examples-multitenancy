package org.amdatu.example.multitenancy.issue;

import java.util.List;

import org.amdatu.example.multitenancy.user.User;

public interface IssueService {

	Issue get(Integer id);
	
	List<Issue> list();
	
	List<Issue> list(User createdBy);
	
	Issue create(User createdBy, String summary, String description);
	
}
