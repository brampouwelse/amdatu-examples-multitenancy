package org.amdatu.example.multitenancy.issue;

import org.amdatu.example.multitenancy.user.User;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Issue {

	public enum Status {
		
		OPEN, 
		
		INPROGRESS, 
		
		CLOSED
		
	}
	
	private final Integer id;
	
	private final User createdBy;

	private final String summary;
	
	private final String description;

	@JsonCreator
	public Issue(@JsonProperty("id") Integer id, @JsonProperty("createdBy") User createdBy, @JsonProperty("summary")String summary, @JsonProperty("description") String description) {
		this.id = id;
		this.createdBy = createdBy;
		this.summary = summary;
		this.description = description;
	}

	public Integer getId() {
		return id;
	}

	public User getCreatedBy() {
		return createdBy;
	}

	public String getSummary() {
		return summary;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Issue other = (Issue) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	} 

}
