angular.module('issueApp', []).controller('IssueController', [
  '$scope',
  '$http',
  function($scope, $http) {

    $scope.selectTenant = function(tenant) {
      $http.get('/' + $scope.tenant.pid + '/user/list')
        .success(function(response) {
          $scope.users = response;
        });

      $http.get('/' + $scope.tenant.pid + '/issue/list')
        .success(function(response) {
          $scope.issues = response;
        });
    }

    $http.get('/tenant/list').success(function(response) {
      $scope.tenants = response;

      $scope.tenant = response[0];
      $scope.selectTenant();
    });

    $scope.addIssue = function() {
      var issue = {
        "createdBy": $scope.createdBy,
        "summary": $scope.summary,
        "description": $scope.description
      };

      $http.post('/' + $scope.tenant.pid + '/issue', issue)
        .success(function(response) {
          $scope.createdBy = null;
          $scope.summary = '';
          $scope.description = '';
          $scope.issues.push(response);
        });
    };
  }
]);
