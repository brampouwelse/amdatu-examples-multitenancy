angular.module('userApp', []).controller(
  'UserController', [
    '$scope',
    '$http',
    function($scope, $http) {

      $scope.selectTenant = function(tenant) {
        $http.get('/' + $scope.tenant.pid + '/user/list')
          .success(function(response) {
            $scope.users = response;
          });
      }

      $http.get('/tenant/list').success(function(response) {
        $scope.tenants = response;
        $scope.tenant = response[0];
        $scope.selectTenant();
      });

      $scope.addUser = function() {
        var user = {
          "userName": $scope.userName,
          "displayName": $scope.displayName
        };

        $http.post('/' + $scope.tenant.pid + '/user', user)
          .success(function() {
            $scope.userName = '';
            $scope.displayName = '';
            $scope.users.push(user);
          });

      };
    }
  ]);
