angular.module('worklogApp', []).controller(
  'WorklogController', [
    '$scope',
    '$http',
    function($scope, $http) {

      $scope.selectTenant = function(tenant) {
        $http.get('/' + $scope.tenant.pid + '/user/list')
          .success(function(response) {
            $scope.users = response;
          });

        $http.get('/' + $scope.tenant.pid + '/issue/list')
          .success(function(response) {
            $scope.issues = response;
          });

        $http.get('/' + $scope.tenant.pid + '/worklog/list')
          .success(function(response) {
            $scope.worklogs = response;
          });

      }

      $http.get('/tenant/list/worklog').success(function(response) {
        $scope.tenants = response;
        $scope.tenant = response[0];
        $scope.selectTenant();
      });

      $scope.addWorklog = function() {
        var worklog = {
          "user": $scope.user,
          "issue": $scope.issue,
          "date": $scope.date,
          "minutesSpent": $scope.minutesSpent
        };

        $http.post('/' + $scope.tenant.pid + '/worklog', worklog)
          .success(function(response) {
            $scope.user = null;
            $scope.issue = null;
            $scope.date = null;
            $scope.minutesSpent = '';
            $scope.worklogs.push(response);
          });

      };
    }
  ]);
