package org.amdatu.example.multitenancy.worklog;

import java.util.Date;
import java.util.List;

public interface WorklogService {

	Worklog add(String userName, Integer issueId, Date date, Integer minutesSpent);
	
	List<Worklog> list(String userName);
	
	List<Worklog> list(Integer issueId);

	List<Worklog> list();
	
	
}
