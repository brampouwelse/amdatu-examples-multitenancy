package org.amdatu.example.multitenancy.worklog;

import java.util.Date;

import org.amdatu.example.multitenancy.issue.Issue;
import org.amdatu.example.multitenancy.user.User;

public class Worklog {

	private final Integer id; 
	
	private final User user; 
	
	private final Issue issue;
	
	private final Date date;
	
	private final Integer minutesSpent;

	public Worklog(Integer id, User user, Issue issue, Date date, Integer minutesSpent) {
		this.id = id;
		this.user = user;
		this.issue = issue;
		this.date = date;
		this.minutesSpent = minutesSpent;
	}
	
	public Worklog(User user, Issue issue, Date date, Integer minutesSpent) {
		this(null, user, issue, date, minutesSpent);
	}

	public Integer getId() {
		return id;
	}
	
	public User getUser() {
		return user;
	}

	public Issue getIssue() {
		return issue;
	}

	public Date getDate() {
		return date;
	}

	public Integer getMinutesSpent() {
		return minutesSpent;
	} 
	
	
	
}
