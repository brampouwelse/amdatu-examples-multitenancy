package org.amdatu.example.multitenancy.worklog.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.amdatu.example.multitenancy.issue.Issue;
import org.amdatu.example.multitenancy.issue.IssueService;
import org.amdatu.example.multitenancy.user.User;
import org.amdatu.example.multitenancy.user.UserService;
import org.amdatu.example.multitenancy.worklog.Worklog;
import org.amdatu.example.multitenancy.worklog.WorklogService;
import org.amdatu.jta.Transactional;

@Transactional
public class JpaWorklogService implements WorklogService {

	private volatile EntityManager entityManager;
	
	private volatile UserService userService;
	
	private volatile IssueService issueService;
	
	@Override
	public Worklog add(String userName, Integer issueId, Date date, Integer minutesSpent) {

		JpaWorklog jpaWorklog = new JpaWorklog(userName, issueId, date, minutesSpent);
		entityManager.persist(jpaWorklog);
		
		return toWorklog(jpaWorklog);
	}

	private Worklog toWorklog(JpaWorklog jpaWorklog) {
		User user = userService.get(jpaWorklog.getUserName());
		Issue issue = issueService.get(jpaWorklog.getIssueId());
		return new Worklog(user, issue, jpaWorklog.getDate(), jpaWorklog.getMinutesWorked());	
	}

	@Override
	public List<Worklog> list() {
		TypedQuery<JpaWorklog> query = entityManager.createQuery("SELECT w FROM JpaWorklog w", JpaWorklog.class);
		
		List<Worklog> worklogs = new ArrayList<Worklog>();
		for (JpaWorklog jpaWorklog : query.getResultList()) {
			worklogs.add(toWorklog(jpaWorklog));
		}
		return worklogs;
	}
	
	@Override
	public List<Worklog> list(String userName) {
		TypedQuery<JpaWorklog> query = entityManager.createQuery("SELECT w FROM JpaWorklog w WHERE w.userName = :userName", JpaWorklog.class);
		query.setParameter("userName", userName);
		
		List<Worklog> worklogs = new ArrayList<Worklog>();
		for (JpaWorklog jpaWorklog : query.getResultList()) {
			worklogs.add(toWorklog(jpaWorklog));
		}
		return worklogs;
	}

	@Override
	public List<Worklog> list(Integer issueId) {
		TypedQuery<JpaWorklog> query = entityManager.createQuery("SELECT w FROM JpaWorklog w WHERE w.issueId = :issueId", JpaWorklog.class);
		query.setParameter("issueId", issueId);
		
		List<Worklog> worklogs = new ArrayList<Worklog>();
		for (JpaWorklog jpaWorklog : query.getResultList()) {
			worklogs.add(toWorklog(jpaWorklog));
		}
		return worklogs;
	}

}
