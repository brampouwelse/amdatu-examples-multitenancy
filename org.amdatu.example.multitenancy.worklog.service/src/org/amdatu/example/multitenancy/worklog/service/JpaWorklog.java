package org.amdatu.example.multitenancy.worklog.service;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class JpaWorklog {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id; 
	
	private String userName; 
	
	private Integer issueId; 
	
	@Temporal(TemporalType.DATE)
	private Date date; 
	
	private Integer minutesWorked;

	public JpaWorklog() {
	
	}
	
	public JpaWorklog(String userName, Integer issueId, Date date, Integer minutesWorked) {
		super();
		this.userName = userName;
		this.issueId = issueId;
		this.date = date;
		this.minutesWorked = minutesWorked;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	public Integer getIssueId() {
		return issueId;
	}

	public void setIssueId(Integer issueId) {
		this.issueId = issueId;
	}

	public Date getDate() {
		return date;
	}
	
	public void setDate(Date date) {
		this.date = date;
	}
	
	public Integer getMinutesWorked() {
		return minutesWorked;
	}

	public void setMinutesWorked(Integer minutesWorked) {
		this.minutesWorked = minutesWorked;
	}
	
	
	
}
