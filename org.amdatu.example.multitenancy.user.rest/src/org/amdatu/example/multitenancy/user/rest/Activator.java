package org.amdatu.example.multitenancy.user.rest;

import org.amdatu.example.multitenancy.user.UserService;
import org.amdatu.multitenant.Tenant;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		dm.add(createComponent().setInterface(Object.class.getName(), null).setImplementation(UserResource.class)
				.add(createServiceDependency().setService(UserService.class).setRequired(true))
				.add(createServiceDependency().setService(Tenant.class).setRequired(true)));

	}

}
