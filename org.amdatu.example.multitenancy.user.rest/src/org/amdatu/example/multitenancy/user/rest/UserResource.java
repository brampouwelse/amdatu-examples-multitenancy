package org.amdatu.example.multitenancy.user.rest;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.example.multitenancy.user.User;
import org.amdatu.example.multitenancy.user.UserService;
import org.amdatu.multitenant.Tenant;

@Path("user")
@Produces(MediaType.APPLICATION_JSON)
public class UserResource {

	private volatile UserService userService;
	
	private volatile Tenant tenant; 
	
	@GET
	public String test(){
		return "Haai " + tenant.getName() + " [" + tenant.getPID() + "]";
	}
	
	@GET
	@Path("list")
	@Produces(MediaType.APPLICATION_JSON)
	public List<User> list(){
		return userService.list();
	}

//	@GET
//	@Path("{userName}")
//	public User get(@PathParam("userName")String userName){
//		return userService.get(userName);
//	}
	
	
	@POST
	public void add(User user){
		userService.add(user);
	}
	
	
	
}
