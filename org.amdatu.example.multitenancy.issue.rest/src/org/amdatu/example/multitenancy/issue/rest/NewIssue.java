package org.amdatu.example.multitenancy.issue.rest;

import org.amdatu.example.multitenancy.user.User;

public class NewIssue {

	private User createdBy;
	private String summary;
	private String description;

	public User getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(User createdBy) {
		this.createdBy = createdBy;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}