package org.amdatu.example.multitenancy.issue.rest;

import org.amdatu.example.multitenancy.issue.IssueService;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.osgi.framework.BundleContext;

public class Activator extends DependencyActivatorBase {

	@Override
	public void destroy(BundleContext arg0, DependencyManager arg1) throws Exception {

	}

	@Override
	public void init(BundleContext arg0, DependencyManager dm) throws Exception {
		dm.add(createComponent().setInterface(Object.class.getName(), null).setImplementation(IssueResource.class)
				.add(createServiceDependency().setService(IssueService.class).setRequired(true)));

	}

}
