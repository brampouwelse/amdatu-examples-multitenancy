package org.amdatu.example.multitenancy.issue.rest;

import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.example.multitenancy.issue.Issue;
import org.amdatu.example.multitenancy.issue.IssueService;

@Path("issue")
@Produces(MediaType.APPLICATION_JSON)
public class IssueResource {

	private volatile IssueService issueService;

	@GET
	@Path("{id}")
	public Issue get(@PathParam("id") Integer id) {
		return issueService.get(id);
	}

	@POST
	public Issue create(NewIssue issue) {
		return issueService.create(issue.getCreatedBy(), issue.getSummary(), issue.getDescription());
	}
	
	@GET
	@Path("list")
	public List<Issue> list(){
		return issueService.list();
	}

}
